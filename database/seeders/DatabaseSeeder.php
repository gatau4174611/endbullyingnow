<?php

namespace Database\Seeders;

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        \App\Models\User::create([
            'username' => 'hendri',
            'password' => Hash::make('hendri00'),
            'phone' => '089670029340',
            'gender' => 'male',
            'email' => 'hendri@gmail.com',
            'level' => 'Administrator',
        ]);

        \App\Models\User::create([
            'username' => 'reymond',
            'password' => Hash::make('reymond'),
            'phone' => '089670029340',
            'gender' => 'male',
            'email' => 'reymond@gmail.com',
            'level' => 'Officers',
        ]);

        \App\Models\User::create([
            'username' => 'jon',
            'password' => Hash::make('jon'),
            'phone' => '089670029340',
            'gender' => 'male',
            'email' => 'jon@gmail.com',
            'level' => 'Student',
        ]);
    }
}
