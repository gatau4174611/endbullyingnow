<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title') | EndBullyingNow</title>

    @stack('prepend-style')
    @include('include.style')
    @stack('addon-style')

    <!-- Start GA -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>
    <!-- /END GA -->

</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <form class="form-inline mr-auto">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i
                                    class="fas fa-bars"></i></a></li>
                        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                    class="fas fa-search"></i></a></li>
                    </ul>

                </form>

                <li class="dropdown"><a href="#" data-toggle="dropdown"
                        class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
                        <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->username }}</div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" method="POST" class="dropdown-item has-icon text-danger"
                            onclick="logout()">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </a>
                    </div>
                </li>
                </ul>
            </nav>
            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="#">EndBullyingNow</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="#">EBN</a>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="menu-header">Dashboard</li>
                        <li class="dropdown active">
                            <a href="{{ route('laporan-bully') }}" class="nav-link"><i
                                    class="fas fa-fire"></i><span>Daftar Laporan</span></a>
                        </li>
                        <li class="dropdown">
                            <a href="{{ route('data-siswa') }}" class="nav-link"><i class="fas fa-columns"></i>
                                <span>Data Siswa</span></a>
                        </li>
                        <li><a class="nav-link" href="{{ route('tanggapan-petugas') }}"><i class="far fa-square"></i>
                                <span>Tanggapan Petugas</span></a></li>
            </div>
        </div>

        @yield('content')

        @include('sweetalert::alert')
        @stack('prepend-script')
        @include('include.script')
        @stack('addon-script')

</body>

</html>
