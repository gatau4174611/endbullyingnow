@extends('layouts.admin')
@section('title', 'Report')
@section('content')

    <!-- Main content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Edit Siswa</h1>
            </div>
            <div class="section-body">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body p-0">
                                <div class="table-responsive p-3">
                                </div>
                                <div class="card-body">
                                    <form action="{{ url('update-siswa', $data->id) }}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" id="username" name="username" class="form-control"
                                                placeholder="Username" value="{{ $data->username }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="text" id="password" name="password" class="form-control"
                                                placeholder="Password" value="{{ $data->password }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Phone</label>
                                            <input type="text" id="phone" name="phone" class="form-control"
                                                placeholder="Phone" value="{{ $data->phone }}">
                                        </div>
                                        <div class="form-group">
                                            <select name="gender" class="form-control selectric">
                                                <option value="">Gender</option>
                                                @foreach (['Male', 'Female'] as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" id="email" name="email" class="form-control"
                                                placeholder="Email" value="{{ $data->email }}">
                                        </div>
                                        <div class="form-group">
                                            <select name="level" class="form-control selectric">
                                                <option value="">Level</option>
                                                @foreach (['Administrator', 'Officers', 'Student'] as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Ubah data</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
