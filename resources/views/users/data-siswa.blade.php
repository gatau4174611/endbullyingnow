@extends('layouts.admin')
@section('title', 'complaint')

@section('content')

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Siswa</h1>
            </div>
            <div class="section-body">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body p-0">
                                <div class="table-responsive p-3">
                                    <table class="table table-striped" id="user">
                                        <thead>
                                            <th>id</th>
                                            <th>Nama Pengguna</th>
                                            <th>Kata Sandi</th>
                                            <th>Nomor Telepon</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Alamat Email</th>
                                            <th>Level</th>
                                            <th>Aksi</th>
                                            <th>Alat</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($user as $i => $sis)
                                                <tr>
                                                    <td>{{ $sis->id }}</td>
                                                    <td>{{ $sis->username }}</td>
                                                    <td>{{ $sis->password }}</td>
                                                    <td>{{ $sis->phone }}</td>
                                                    <td>{{ $sis->gender }}</td>
                                                    <td>{{ $sis->email }}</td>
                                                    <td>{{ $sis->level }}</td>
                                                    <td>
                                                        <a href="{{ route('edit-siswa', $sis->id) }}"
                                                            class="btn btn-secondary">Edit</a>
                                                    </td>
                                                    <td>
                                                        <form action="{{ url('delete-siswa', $sis->id) }}" class="d-inline"
                                                            method="POST" id="delete-form-{{ $sis->id }}">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="button" class="btn btn-danger"
                                                                onclick="deleteData('{{ $sis->id }}')">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <div class="card-tools ml-auto">
                                        <button onclick="window.print()" class="btn btn-default"><i
                                                class="fas fa-print"></i>
                                            Print</button>
                                        <a href="{{ route('create-siswa') }}" class="btn btn-primary"><i
                                                class="fas fa-plus"></i> Tambah
                                            Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

@endsection

@push('addon-script')
    <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.3/js/dataTables.bootstrap5.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $('#complaintTable').DataTable()
        })


        function deleteData(id) {
            Swal.fire({
                title: 'PERINGATAN!',
                text: 'YAKIN INGIN MENGHAPUS?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yakin',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.value) {
                    $('#delete-form-' + id).submit();
                }
            });
        }
    </script>
@endpush
