@extends('layouts.admin')
@section('title', 'Tambah Siswa')
@section('content')

    <!-- Main content -->
    <div class="main-content">
        <div id="success-message"></div>
        <section class="section">
            <div class="section-header">
                <h1>Tambah Siswa</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body p-0">
                                <div class="table-responsive p-3"></div>
                                <div class="card-body">
                                    <form action="{{ route('simpan-siswa') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="text" id="username" name="username" class="form-control"
                                                placeholder="Username">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="password" name="password" class="form-control"
                                                placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="phone" name="phone" class="form-control"
                                                placeholder="Phone">
                                        </div>
                                        <div class="form-group">
                                            <select name="gender" class="form-control selectric">
                                                <option value="">Gender</option>
                                                @foreach (['Male', 'Female'] as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="email" name="email" class="form-control"
                                                placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <select name="level" class="form-control selectric">
                                                <option value="">Level</option>
                                                @foreach (['Administrator', 'Officers', 'Student'] as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

<script>
    // setelah berhasil menambahkan siswa
    document.getElementById("success-message").innerHTML = "Siswa berhasil ditambahkan";

    // tambahkan kelas CSS untuk animasi
    document.getElementById("success-message").classList.add("success-animation");

    setTimeout(function() {
        document.getElementById("success-message").innerHTML = "";
        document.getElementById("success-message").classList.remove("success-animation");
    }, 3000); // menghilangkan pesan sukses setelah 3 detik
</script>

<style>
    #success-message {
        display: none;
        /* styling lainnya */
    }

    .success-animation {
        animation-name: success;
        animation-duration: 1s;
    }

    @keyframes success {
        0% {
            opacity: 0;
            transform: translateY(-10px);
        }

        50% {
            opacity: 1;
            transform: translateY(0);
        }

        100% {
            opacity: 0;
            transform: translateY(10px);
        }
    }
</style>
