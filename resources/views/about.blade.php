<!DOCTYPE html>
<html>

<head>
    <title>EndBullyingNow | Student</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Serif&display=swap');
    </style>
</head>

<body>

    <div class="kmj-about" id="about">
        <div class="glasseffect --blue" id="join">
            <div class="row">
                <div class="col-sm-12">
                    <button class="btn btn-outline-dark" onclick="window.history.back()">Kembali</button>

                    <div class="page-title about__title">
                        <h1><strong>Apa itu Bullying</strong></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMission">Tentang
                                        Bullying</a>
                                </h4>
                            </div>
                            <div id="collapseMission" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <strong>Bullying (pembullyan) adalah tindakan yang dilakukan oleh seseorang atau
                                        sekelompok orang untuk menyakiti, merendahkan,
                                        atau mengeksploitasi orang lain secara terus-menerus, baik secara fisik, verbal,
                                        atau psikologis. Bullying dapat terjadi
                                        di berbagai lingkungan, seperti di sekolah, tempat kerja, atau lingkungan
                                        online.
                                        <br>
                                        <br>
                                        Bullying dapat memiliki dampak yang sangat negatif pada kesehatan mental
                                        dan fisik korban. Korban bullying seringkali
                                        merasa rendah diri, takut, dan cemas. Mereka mungkin mengalami kesulitan
                                        dalam belajar atau bekerja, dan bahkan dapat
                                        mengalami depresi atau gangguan kecemasan yang lebih serius.
                                        <br>
                                        <br>
                                        Penting untuk mengambil tindakan untuk mencegah dan menangani bullying. Ini
                                        dapat mencakup mempromosikan kesadaran dan
                                        pendidikan tentang bullying, memberikan sumber daya dan dukungan untuk korban,
                                        serta mengembangkan kebijakan dan
                                        prosedur yang jelas untuk menangani bullying. Semua orang memiliki peran penting
                                        dalam memerangi bullying, baik itu
                                        sebagai individu, keluarga, sekolah, atau masyarakat secara keseluruhan.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#collapseMission"
                                        href="#collapseVision">Jenis-jenis
                                        Bullying</a>
                                </h4>
                            </div>
                            <div id="collapseVision" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Berikut ini adalah beberapa macam perundungan atau bullying yang dapat terjadi:
                                        <br>
                                        <br>
                                        1. Bullying fisik: Meliputi tindakan-tindakan seperti menendang, memukul,
                                        merobek
                                        pakaian, atau merusak barang milik orang
                                        lain secara paksa.
                                        <br><br>
                                        2. Bullying verbal: Meliputi penggunaan kata-kata yang menyakitkan, menghina,
                                        atau
                                        melecehkan seseorang, seperti
                                        mengolok-olok atau memaki.
                                        <br><br>
                                        3. Bullying psikologis: Meliputi tindakan-tindakan yang membuat korban merasa
                                        terisolasi, tidak berdaya, atau diabaikan, <br>
                                        seperti tidak diajak bergabung dalam kegiatan, rumor atau gossip negatif, atau
                                        mengancam agar korban tidak memberi tahu
                                        siapapun tentang apa yang terjadi.
                                        <br><br>
                                        4. Cyberbullying: Meliputi tindakan-tindakan bullying yang terjadi melalui media
                                        sosial atau platform online lainnya,
                                        seperti mengirim pesan yang menghina atau memposting konten yang merendahkan.
                                        <br><br>
                                        5. Bullying seksual: Meliputi tindakan-tindakan yang merendahkan atau
                                        mempermalukan
                                        korban secara seksual, seperti meraba
                                        atau mencolek bagian tubuh korban secara tidak pantas, atau memaksakan perilaku
                                        seksual yang tidak diinginkan.
                                        <br><br>
                                        6. Bullying rasial atau diskriminatif: Meliputi tindakan-tindakan yang bersifat
                                        diskriminatif terhadap kelompok tertentu,
                                        seperti mengejek atau mengolok-olok seseorang karena ras, agama, atau orientasi
                                        seksual.
                                        <br><br>
                                        7. Bullying di tempat kerja: Meliputi tindakan-tindakan bullying yang terjadi di
                                        lingkungan kerja, seperti pengabaian,
                                        diskriminasi, atau pelecehan seksual yang dilakukan oleh rekan kerja atau
                                        atasan.
                                        <br>
                                        <br>
                                        <br>
                                        Semua bentuk perundungan memiliki dampak yang negatif dan dapat membahayakan
                                        kesehatan mental dan fisik korban. Penting
                                        untuk mencegah dan menangani perundungan dengan serius, baik di lingkungan
                                        sekolah, tempat kerja, atau lingkungan
                                        online.
                                    </p>
                                </div>

                                <style>
                                    .kmj-about {
                                        position: relative;
                                        padding: 80px 5vw 50px 5vw;
                                        padding-bottom: 10%;
                                        width: 100%;
                                        height: auto;
                                        background: white;
                                    }

                                    #about .row {
                                        position: relative;
                                        display: block;
                                    }

                                    #about .col-sm-4 {
                                        position: relative;
                                    }

                                    #about #accordion,
                                    #about #accordion2 {
                                        margin-bottom: 0;
                                        display: block;
                                        width: 100%;
                                    }

                                    #about .panel-default {
                                        background-color: transparent !important;
                                        border: none !important;
                                        text-align: center;
                                    }

                                    #about .panel-heading {
                                        background: rgba(255, 255, 255, 0.75);
                                        border-radius: 10px;
                                    }

                                    #about .panel-title>a {
                                        color: rgb(0, 0, 0);
                                        font-size: 25px;
                                        font-weight: 600;
                                    }

                                    #about .panel-title>a:hover,
                                    #about .panel-title>a:focus {
                                        text-decoration: none;
                                    }

                                    #about .panel-body {
                                        color: #585F54;
                                        font-size: 20px;
                                        font-family: 'IBM Plex Serif', serif;
                                    }

                                    .container-img>img {
                                        width: 100%;
                                    }

                                    .button-container {
                                        display: flex;
                                        justify-content: flex-end;
                                        gap: 10px;
                                    }
                                </style>
