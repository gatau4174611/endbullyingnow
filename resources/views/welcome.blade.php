<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EndBullyingNow</title>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    <!-- Styles -->
    <style>
        a {
            color: inherit;
            text-decoration: inherit;
            font-size: 20px;
        }

        .p-6 {
            padding: 1.5rem;
        }

        .px-6 {
            padding-left: 1.5rem;
            padding-right: 1.5rem;
        }

        .text-right {
            text-align: right;
        }

        .font-semibold {
            font-weight: 600;
        }

        .background {
            background-image: url("../assets/img/7.png");
            background-size: cover;
            background-position: bottom;
            background-repeat: no-repeat;
            height: 55rem;
        }
    </style>
</head>

<body class="antialiased">
    <div
        class="relative sm:flex sm:justify-center sm:items-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white">
        @if (Route::has('login'))
            <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right">
                @auth
                    <a href="{{ url('/home') }}"
                        class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Home</a>
                @else
                    <a href="{{ route('login') }}"
                        class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log
                        in</a>
                @endauth
            </div>
            <div class="background"></div>
        @endif
        <div class="main_footer">
            <div class="banner_img"></div>
            <div class="container footer">
                <div class="footer__logo">
                    EndBullyingNow
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<style>
    @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap");

    :root,
    body,
    html {
        font-family: "Poppins", sans-serif;
        font-size: 12px;
        margin: 0;
        padding: 0;
        letter-spacing: 0.02em;
        line-height: 1.2rem;
        background-color: #f1f1f1;
    }

    :root::-webkit-scrollbar,
    body::-webkit-scrollbar,
    html::-webkit-scrollbar {
        display: none;
    }

    :root ::-moz-selection,
    body ::-moz-selection,
    html ::-moz-selection {
        background-color: #11939a;
        color: #f1f1f1;
        padding: 0;
    }

    :root ::selection,
    body ::selection,
    html ::selection {
        background-color: #11939a;
        color: #f1f1f1;
        padding: 0;
    }

    .container {
        max-width: 100%;
        padding: 0 3rem;
    }

    @media (max-width: 770px) {
        .container {
            padding: 0 1rem;
        }
    }

    .main_footer {
        position: relative;
        color: #f1f1f1;
        font-weight: 300;
    }

    @media screen and (max-width: 770px),
    screen and (min-height: 752px) {
        .main_footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
    }

    @media screen and (max-width: 770px) {
        .main_footer .banner_img {
            width: 100%;
            background-size: 200vw;
            position: absolute;
            bottom: 10rem;
            z-index: 1;
        }
    }

    .main_footer *.footer {
        background-color: #000;
    }

    .main_footer *.footer__logo {
        display: block;
        font-size: 2.4rem;
        font-weight: 500;
        letter-spacing: -1px;
        padding: 2rem 0;
    }

    @media (max-width: 770px) {
        .main_footer *.footer__logo {
            color: #000;
        }
    }

    .main_footer *.footer__devider {
        width: 100%;
        border-bottom: solid rgba(241, 241, 241, 0.5) 0.5px;
    }

    .main_footer *.footer__content {
        padding: 2rem 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        font-size: 0.85rem;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .main_footer *.footer__content .col span:nth-child(1) {
        margin-right: 0.3rem;
    }

    .main_footer *.footer__content .col span:nth-last-child(1) {
        margin-left: 0.3rem;
    }
</style>
