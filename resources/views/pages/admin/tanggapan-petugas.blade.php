@extends('layouts.admin')
@section('title', 'Report')
@section('content')

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>

    <!-- Main content -->

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Tanggapan</h1>
            </div>
            <div class="section-body">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body p-0">
                                <div class="table-responsive p-3">
                                    <table class="table table-striped" id="complaintTable">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>school_class</th>
                                                <th>report_title</th>
                                                <th>report_detail</th>
                                                <th>incident_time</th>
                                                <th>place</th>
                                                <th>type_of_bullying</th>
                                                <th>victim_name</th>
                                                <th>class</th>
                                                <th>reporter_id</th>
                                                <th>proof</th>
                                                <th>responses</th>
                                                <th>verification</th>
                                            </tr>
                                            @foreach ($complaint as $i => $sis)
                                                <tr>
                                                    <td>{{ $sis->id }}</td>
                                                    <td>{{ $sis->school_class }}</td>
                                                    <td>{{ $sis->report_title }}</td>
                                                    <td>{{ $sis->report_detail }}</td>
                                                    <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                                                    <td>{{ $sis->place }}</td>
                                                    <td>{{ $sis->type_of_bullying }}</td>
                                                    <td>{{ $sis->victim_name }}</td>
                                                    <td>{{ $sis->class }}</td>
                                                    <td>{{ $sis->reporter_id }}</td>
                                                    <td>{{ $sis->proof }}</td>

                                                    <td>
                                                        <form method="POST"
                                                            action="{{ route('perundungan.update', ['id' => $sis->id]) }}">
                                                            @csrf
                                                            @method('PUT')
                                                            <div class="form-group">
                                                                <select class="form-control selectric" name="responses">
                                                                    <option value="Confirmed"
                                                                        {{ $sis->responses == 'Confirmed' ? 'selected' : '' }}>
                                                                        Confirmed</option>
                                                                    <option value="Hoax"
                                                                        {{ $sis->responses == 'Hoax' ? 'selected' : '' }}>
                                                                        Hoax</option>
                                                                </select>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary">Update
                                                                Responses</button>
                                                        </form>
                                                    </td>

                                                    <td>
                                                        <form method="POST"
                                                            action="{{ route('perundungan.update', ['id' => $sis->id]) }}">
                                                            @csrf
                                                            @method('PUT')
                                                            <div class="form-group">
                                                                <select class="form-control selectric" name="verification">
                                                                    <option value="waiting"
                                                                        {{ $sis->verification == 'waiting' ? 'selected' : '' }}>
                                                                        Waiting</option>
                                                                    <option value="process"
                                                                        {{ $sis->verification == 'process' ? 'selected' : '' }}>
                                                                        Process</option>
                                                                    <option value="finished"
                                                                        {{ $sis->verification == 'finished' ? 'selected' : '' }}>
                                                                        Finished</option>
                                                                </select>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary">Update
                                                                Verification</button>
                                                        </form>
                                                    </td>

                                                    <td>
                                                        <form action="{{ url('delete-laporan', $sis->id) }}"
                                                            class="d-inline" method="POST"
                                                            id="delete-form-{{ $sis->id }}">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="button" class="btn btn-danger"
                                                                onclick="deleteData('{{ $sis->id }}')">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endsection

        @push('addon-script')
            <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.13.3/js/dataTables.bootstrap5.min.js"></script>

            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
            <script>
                $(document).ready(function() {
                    $('#complaintTable').DataTable()
                })


                function deleteData(id) {
                    Swal.fire({
                        title: 'PERINGATAN!',
                        text: 'YAKIN INGIN MENGHAPUS?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yakin',
                        cancelButtonText: 'Batal',
                    }).then((result) => {
                        if (result.value) {
                            $('#delete-form-' + id).submit();
                        }
                    });
                }
            </script>
        @endpush
