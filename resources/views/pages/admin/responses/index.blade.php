@extends('layouts.admin')
@section('title', 'complaint')

@section('content')

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>


      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>complaint</h1>
          </div>
          <div class="section-body">
            <div class="row">
            </div>
            <div class="row">
              <div class="col-12">
                <div class="card">

                  <div class="card-body p-0">
                    <div class="table-responsive p-3">
                      <table class="table table-striped" id="complaintTable">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>VICTIMS NAME</th>
                            <th>GRADE</th>
                            <th>TITLE</th>
                            <th>TYPE OF BULYING</th>
                            <th>TIME HAPPEND</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($complaint as $i => $com)
                            <tr>
                              <td>{{ $i += 1 }}</td>
                              <td>{{ $com -> victim_name }}</td>
                              <td>{{ $com -> grade }}</td>
                              <td>{{ $com -> report_title }}</td>
                              <td>{{ $com -> type_of_bullying }}</td>
                              <td>{{ $com -> incident_time }}</td>
                              <td>
                                <a href="{{ route('data-complaint.edit', $com->id) }}" class="btn btn-primary">Edit </a>
                              </td>
                              <td>
                                <form action="{{ url('data-complaint', $com->id) }}" class="d-inline" method="POST" id="delete{{$com->id}}">
                                  @csrf
                                  @method('delete')
                                  <button type="button" class="btn btn-danger" onclick="deleteData('{{$com->id}}')">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-header float-right">
            <a href="/data-complaint/create" class="btn btn-danger">Tambah</a>
            <div class="card-header-action">
            </div>
          </div>
        </section>
      </div>
@endsection

@push('addon-script')
<script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.3/js/dataTables.bootstrap5.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  $(document).ready(function(){
      $('#complaintTable').DataTable()
  })


  function deleteData(id){
    Swal.fire({
      title: 'PERINGATAN!',
      text: 'YAKIN INGIN MENGHAPUS?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yakin',
      cancelButtonText: 'Batal',

    }).then((result) => {
      if(result.value) {
        $('#delete' +id).submit();
      }
    });

  }
</script>
@endpush