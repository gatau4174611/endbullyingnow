@extends('layouts.admin')
@section('title', 'Report')
@section('content')

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <div class="section-header-back">
                    <a href="{{ url('home') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
                </div>
                <h1>Kembali</h1>

            </div>

            <div class="section-body">
                <h2 class="section-title">Sampaikan Laporan Mu</h2>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Write Your Post</h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('data-complaint.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul
                                            Laporan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" value="{{ old('id') }}" required
                                                name="report_title">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Detail
                                            Laporan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" name="detail" required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Waktu
                                            Kejadian</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="datetime-local" class="form-control" value="{{ old('id') }}"
                                                required name="incident_time">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tempat
                                            Kejadian</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" value="{{ old('id') }}" required
                                                name="place">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis
                                            Perundungan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control selectric" name="type_of_bullying">
                                                @foreach ($type_of_bullying as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="card-header">
                                        <h4>Data Korban</h4>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama
                                            Korban</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" value="{{ old('id') }}" required
                                                name="victim_name">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat
                                            Pendidikan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control selectric" name="school_class">
                                                @foreach ($class as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis
                                            Kelamin</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control selectric" value="{{ old('id') }}" required
                                                name="jenis_kelamin">
                                                @foreach ($gender as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="card-header">
                                        <h4>Data Pelapor</h4>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id
                                            Pelapor</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select name="reporter_id" class="form-control">
                                                @foreach ($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->id }} -
                                                        {{ $user->username }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat
                                            Pendidikan</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control selectric" name="class">
                                                @foreach ($class as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bukti</label>
                                        <div class="col-sm-12 col-md-7">
                                            <div id="image-preview" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="file" name="proof" id="image-upload" optional
                                                    name="proof">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('kirim') }}
                                        </button>
                                    </div>
                            </div>
                            </form>
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <p class="text-danger">{{ $error }}</p>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
        </section>
    </div>
    </div>
    </div>
@endsection
