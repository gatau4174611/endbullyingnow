@extends('layouts.admin')
@section('title', 'edit')
@section('content')


    
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <div class="section-header-back">
              <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Create New Post</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Posts</a></div>
              <div class="breadcrumb-item">Create New Post</div>
            </div>
          </div>

          <div class="section-body">
            <h2 class="section-title">Create New Post</h2>
            <p class="section-lead">
              On this page you can create a new post and fill in all fields.
            </p>

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Write Your Post</h4>
                  </div>
                  <div class="card-body">
                  <form action="{{ route('data-complaint.update', $complaint->status) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                    </div>
                  </form>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ $complaint->status }}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Detail Laporan</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote-simple" value="{{ old('id')}}" required></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Waktu Kejadian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tempat</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Perundungan</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric">
                          <option>-</option>
                          <option>Physical bullying</option>
                          <option>Verbal bullying</option>
                          <option>Social bullying</option>
                          <option>Psychological bullying</option>
                          <option>Cyberbullying</option>
                          <option>Sexual bullying</option>
                        </select>
                      </div>
                    </div>
                    <div class="card-header">
                      <h4>Data Korban</h4>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Korban</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat Pendidikan</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric">
                          <option>-</option>
                          <option>SD</option>
                          <option>SMP</option>
                          <option>SMK</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kelas</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Kelamin</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric" value="{{ old('id')}}" required>
                          <option>-</option>
                          <option>Pria</option>
                          <option>Wanita</option>
                        </select>
                      </div>
                    </div>

                    <div class="card-header">
                      <h4>Data Pelapor</h4>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Pelapor</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tingkat Pendidikan</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric">
                          <option>-</option>
                          <option>SD</option>
                          <option>SMP</option>
                          <option>SMK</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kelas</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Kelamin</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric">
                          <option>-</option>
                          <option>Pria</option>
                          <option>Wanita</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nomor Telepon</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" value="{{ old('id')}}" required>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bukti</label>
                      <div class="col-sm-12 col-md-7">
                        <div id="image-preview" class="image-preview">
                          <label for="image-upload" id="image-label">Choose File</label>
                          <input type="file" name="image" id="image-upload" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Detail Bukti</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote-simple"></textarea>
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  @endsection