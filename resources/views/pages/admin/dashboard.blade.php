@extends('layouts.admin')
@section('title', 'Dashboard')
@section('content')

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1></h1>
            </div>
            <div class="section-body">
            </div>
            <div class="card-body">
                <h5>Ingatlah bahwa pekerjaanmu bukan hanya sekadar sebuah pekerjaan, tapi juga merupakan tanggung jawab
                    sosialmu. Oleh karena itu, lakukanlah pekerjaanmu dengan baik dan jujur, demi kebaikan bersama.
                </h5>
            </div>
            <div class="card-header float-right">
                <a href="{{ route('data-complaint.create') }}" class="btn btn-danger">LAPOR!</a>
                <div class="card-header-action">
                </div>
            </div>
    </div>
    </div>
    </section>
    </div>

    </section>
    </div>

@endsection
