<!DOCTYPE html>
<html>

<head>
    <title>EndBullyingNow | Student</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=IBM+Plex+Serif&display=swap');
    </style>
</head>

<body>

    <!-- ###############################	HOME PAGE 	###############################	-->

    <nav class="navbar navbar-default navbar-fixed-top" id="myNavbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="Index.html" class="navbar-brand">
                    <img src="../assets/img/EndBullyingNow.png" alt="">
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#home"><b>Home</b></a></li>
                <li><a href="#about"><b>About</b></a></li>
                <li>
                    <form action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger  navbar-btn">
                            <i class="fa fa-sign-out"></i>
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="kmj-home" id="home">
        <div class="slogan outer">
            <div class="middle">
                <div class="inner">
                    <p><strong></strong></p>
                </div>
            </div>
        </div>
    </div>

    <!-- ###############################	/HOME PAGE 	 ###############################	-->

    <!-- ###############################	TENTANG KAMI   ###############################	-->
    <div class="kmj-about" id="about">
        <div class="glasseffect --blue" id="join">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title about__title">
                        <h1><strong>Apa itu EndBullyingNow</strong></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseMission"></a>
                                </h4>
                            </div>
                            <div id="collapseMission" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <strong>EndBullyingNow</strong> adalah Aplikasi Website yang berfungsi sebagai
                                    platform pengaduan bullying atau perundungan yang terjadi disekolah, <br> Melalui
                                    EndBullyingNow,
                                    kami ingin menginspirasi orang-orang untuk berbicara dan bertindak melawan bullying.
                                    Setiap tindakan kecil dapat membuat perbedaan besar, dan kami percaya bahwa
                                    bersama-sama kita bisa mengakhiri bullying.
                                    <br>
                                    Mari bersama-sama mengakhiri bullying!, kami berharap dengan adanya platform
                                    pengaduan ini menjadikan sekolah lebih aman, damai, dan tentram bagi semua orang
                                </div>
                            </div>
                        </div>

                        <div class="button-container">
                            <button class="report-button"
                                onclick="window.location.href='create-pengaduan'">Lapor</button>
                            <button class="what-is-bullying-button" onclick="window.location.href='/about'">Apa
                                itu
                                Bullying?</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="cards-list">

            <div class="card 1">
                <div class="card_image"> <img src="../assets/img/1.png" /> </div>

            </div>

            <div class="card 2">
                <div class="card_image">
                    <img src="../assets/img/2.png" />
                </div>

            </div>

            <div class="card 3">
                <div class="card_image">
                    <img src="../assets/img/3.png" />
                </div>

            </div>

        </div>
    </div>

    <div>
        <div class="cards-list">

            <div class="card 1">
                <div class="card_image"> <img src="../assets/img/4.png" /> </div>

            </div>

            <div class="card 2">
                <div class="card_image">
                    <img src="../assets/img/5.png" />
                </div>

            </div>

            <div class="card 3">
                <div class="card_image">
                    <img src="../assets/img/6.png" />
                </div>

            </div>

        </div>
    </div>

    <div>
        <div class="cards-list">

        </div>
    </div>

    </div>
    </div>

    <!-- ###############################	/PRODUK 	###############################		-->

    <!-- ###############################	KONTAK			###############################	-->

    <!-- ###############################	/KONTAK			###############################	-->

    <script>
        $(document).ready(function() {
            // Add scrollspy to <body>
            $('body').scrollspy({
                target: ".kmj-home",
                offset: 50
            });

            // Add smooth scrolling on all links inside the navbar
            $("#myNavbar a, #navFooter a").on('click', function(event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function() {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });
        });

        $(window).scroll(function() {
            if ($(document).scrollTop() > 100) {
                $('#myNavbar').addClass('transparent');
            } else {
                $('#myNavbar').removeClass('transparent');
            }
        });
    </script>

    <style>
        * {
            margin: 0;
            padding: 0;
            transition: all .5s;
            box-sizing: border-box;
        }

        body {
            position: relative;
            width: 100%;
        }

        .navbar-default {
            background-color: #ffffff !important;
            border: none !important;
            z-index: 98;
            transition: all .5s ease;
            min-width: 1100px;
        }

        nav.transparent {
            background: #FAFAFA
                /*#000000*/
                 !important;
            box-shadow: 1px 4px 4px rgba(0, 0, 0, 0.116);
        }

        .transparent .navbar-nav>li>a {
            color: black !important;
        }

        .navbar-header .navbar-brand {
            position: absolute;
            top: -10px;
            left: 80px;
        }

        .navbar-header .navbar-brand>img {
            width: 300px;
        }

        .navbar .container-fluid .navbar-nav {
            margin-right: 55px;
        }

        .navbar .container-fluid .navbar-nav>li>a {
            position: relative;
            padding: 25px;
            color: rgb(78, 78, 78);
            font-size: 20px;
            transition: all 0s;
            text-transform: uppercase;
            letter-spacing: 2px;
            transition: background-color .5s ease;
            font-family: Arial;
        }

        .navbar .container-fluid .navbar-nav>li>a:hover,
        .navbar .container-fluid .navbar-nav>li>a:focus {
            background-color: rgba(0, 0, 0, 0.027);
        }

        .navbar .container-fluid .navbar-nav>li>a:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            width: 0%;
            height: 3px;
            background-color: #000000;
            transition: width .5s ease;
        }

        .navbar .container-fluid .navbar-nav>li>a:hover:before {
            width: 100%;
        }

        .button[type="submit"] {
            background-color: #d9534f;
            border-color: #d43f3a;
            color: #fff;
            font-size: 14px;
            border-radius: 4px;
            margin-left: 50px;
        }

        .button[type="submit"]:hover {
            background-color: #c9302c;
            border-color: #ac2925;
        }

        .mobile-nav {
            display: none;
            visibility: hidden;
        }

        .kmj-home {
            position: relative;
            background: white url("../assets/img/bg.png") no-repeat center top;
            background-size: cover;
        }

        /* ==============  ==============*/
        .kmj-homeintro {
            position: relative;
            background-color: #EAEAEA;
            width: 100%;
            padding: 25px 0;
            font-size: 16px;
            letter-spacing: 1px;
        }

        .kmj-homeintro ol {
            margin-left: 15px;
        }

        .outer {
            display: table;
            position: relative;
            height: 100vh;
            width: 100%;
        }

        .middle {
            display: table-cell;
            vertical-align: middle;
            text-align: left;
            /* center */
        }

        .inner {
            margin-left: auto;
            margin-right: auto;
            padding: 0 50% 0 50px;
            /* 0 50px */
            width: 100%;
            /*whatever width you want*/
            font-size: 75px;
            letter-spacing: 1px;
            text-transform: uppercase;
        }

        .kmj-home .slogan {
            color: rgb(0, 0, 0);
            background-color: rgba(255, 255, 255, 0);
            font-family: "sans-serif";
        }

        /* ==============  ==============*/
        .kmj-about {
            position: relative;
            padding: 80px 5vw 50px 5vw;
            padding-bottom: 10%;
            width: 100%;
            height: auto;
            background: white;
        }

        #about .row {
            position: relative;
            display: block;
        }

        #about .col-sm-4 {
            position: relative;
        }

        #about #accordion,
        #about #accordion2 {
            margin-bottom: 0;
            display: block;
            width: 100%;
        }

        #about .panel-default {
            background-color: transparent !important;
            border: none !important;
            text-align: center;
        }

        #about .panel-heading {
            background: rgba(255, 255, 255, 0.75);
            border-radius: 10px;
        }

        #about .panel-title>a {
            color: rgb(0, 0, 0);
            font-size: 25px;
            font-weight: 600;
        }

        #about .panel-title>a:hover,
        #about .panel-title>a:focus {
            text-decoration: none;
        }

        #about .panel-body {
            color: #585F54;
            font-size: 20px;
            font-family: 'IBM Plex Serif', serif;
        }

        .container-img>img {
            width: 100%;
        }

        .button-container {
            display: flex;
            justify-content: flex-end;
            gap: 10px;
        }

        .report-button {
            background-color: red;
            color: white;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        .what-is-bullying-button {
            background-color: blue;
            color: white;
            padding: 10px 20px;
            border-radius: 5px;
            border: none;
            cursor: pointer;
        }

        /* ==============  ==============*/
        .kmj-product {
            padding: 10px 5vw 50px 5vw;
            height: 100%;
            background: rgba(231, 197, 124, 0) url("../Images/Elegant-Pattern-PNG-Transparent-Image.png");
            background-size: cover;
        }

        .product-title {
            margin-bottom: 15px;
            font-weight: 700;
        }

        .cards-list {
            z-index: 0;
            width: 100%;
            display: flex;
            justify-content: space-around;
            flex-wrap: wrap;
        }

        .card {
            margin: 30px auto;
            width: 300px;
            height: 300px;
            border-radius: 40px;
            box-shadow: 5px 5px 30px 7px rgba(0, 0, 0, 0.1), -5px -5px 30px 7px rgba(0, 0, 0, 0.12);
            cursor: pointer;
            transition: 0.4s;
        }

        .card .card_image {
            width: inherit;
            height: inherit;
            border-radius: 40px;
        }

        .card .card_image img {
            width: inherit;
            height: inherit;
            border-radius: 40px;
            object-fit: cover;
        }

        .card .card_title {
            text-align: center;
            border-radius: 0px 0px 40px 40px;
            font-family: sans-serif;
            font-weight: bold;
            font-size: 30px;
            margin-top: -55px;
            height: 40px;
        }

        .card:hover {
            transform: scale(0.9, 0.9);
            box-shadow: 5px 5px 30px 15px rgba(0, 0, 0, 0.15),
                -5px -5px 30px 15px rgba(0, 0, 0, 0.18);
        }

        .title-white {
            color: white;
        }

        .title-black {
            color: black;
        }

        @media all and (max-width: 500px) {
            .card-list {
                /* On small screens, we are no longer using row direction but column */
                flex-direction: column;
            }
        }

        /* hiuhfikehfksef */
        /*#########################		PRODUK DETAIL	###########################*/
        .produk-kembali {
            position: absolute;
            z-index: 99;
            top: 25px;
            left: 65px;
        }

        .product {
            position: relative;
            padding: 10vh 0;
            width: 100%;
            height: 100%;
        }

        .product img {
            margin-top: 25px;
            width: 450px;
            padding-right: 16vw;
        }

        .product .konten {
            padding: 7vh 5vw 5vh 10vw;
            line-height: 26px;
            font-size: 17px;
            font-family: 'bodoni', sans-serif;
        }

        .product .konten h1 {
            font-family: 'Hind Siliguri', sans-serif;
            font-weight: 700;
        }

        .product-footer {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            margin: 0;
            padding: 10px 0 0 0;
            background: white;
        }

        .product-footer p {
            display: block;
            font-family: 'Raleway', sans-serif;
            font-size: 16px;
            text-align: center;
            color: gray;
        }

        /* ==============  ==============*/
        .kmj-contact {
            background-color: #f7f7f7;
            padding: 100px 80px 50px 80px;
            width: 100%;
            height: auto;
        }

        .contact__title {
            margin-left: 50px;
            margin-bottom: 50px;
        }

        .contact-intro>p {
            color: #c58d48;
            font-size: 1.25vw;
            font-family: 'IBM Plex Serif', serif;
        }

        #contact .form-group>input[type="text"],
        #contact .form-group>input[type="email"],
        #contact .form-group>textarea {
            background-color: transparent;
            border: 2px solid #eac292;
            border-radius: 2px;
            font-size: 15px;
            font-weight: 600;
            color: #585F54;
        }

        #contact .form-group>input[type="text"],
        #contact .form-group>input[type="email"] {
            height: 40px;
        }

        #contact .action {
            text-align: right;
        }

        #contact .action>label {
            width: 25%;
            background-color: #eac292;
            border: none;
            border-top-left-radius: 25px;
            border-top-right-radius: 0;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 25px;
            color: rgb(255, 255, 255);
            font-size: 1.25vw;
            font-weight: 600;
        }

        .container-navbar-footer {
            margin: 50px 0;
            width: 100%;
            display: block;
        }

        ul.navbar-footer {
            text-align: center;
            list-style: none;
        }

        ul.navbar-footer>li>a {
            padding: 25px;
            font-size: 1.25vw;
            font-weight: 600;
            color: #eac292;
        }

        ul.navbar-footer>li>a:hover,
        ul.navbar-footer>li>a:focus {
            text-decoration: none;
        }

        .container-follow-us-footer {
            margin: 10px 0;
            width: 100%;
            display: block;
        }

        ul.follow-us-btn {
            text-align: center;
            list-style: none;
        }

        ul.follow-us-btn>li>a {
            font-size: 2.5vw;
            color: white;
            margin: 0 10px;
        }

        ul.follow-us-btn>li>a:hover {
            text-decoration: none;
        }

        ul.follow-us-btn>li>.fa-facebook-official:hover {
            color: #3C5B9B;
        }

        ul.follow-us-btn>li>.fa-twitter:hover {
            color: #00CCFF;
        }

        ul.follow-us-btn>li>.fa-instagram:hover {
            color: #F05B45;
        }

        ul.follow-us-btn>li>.fa-linkedin:hover {
            color: #0172B1;
        }

        ul.follow-us-btn>li>.fa-youtube:hover {
            color: #CC3334;
        }

        @media screen and (max-width: 480px) {
            .navbar-default {
                display: none;
                visibility: hidden;
            }

            .mobile-nav {
                display: block;
                visibility: visible;
                position: fixed;
                top: 0;
                left: 0;
                background-color: rgba(255, 255, 255, .95);
                z-index: 98;
            }

            .mobile-nav>.full-width-nav {
                width: 90%;
            }

            .mobile-nav>.full-width-nav img {
                width: 20%;
                display: block;
                margin-left: auto;
                margin-right: auto;
            }

            .mobile-nav>.full-width-nav>ul.right-nav {
                display: inline-block;
                float: right;
            }

            .mobile-nav ul.right-nav>li {
                list-style: none;
                display: inline;
                float: left;
                padding-top: 17px;
            }

            .mobile-nav ul.right-nav>li>a {
                font-size: 15px;
                color: black;
                padding: 0 5px;
                text-transform: uppercase;
            }

            .mobile-nav ul.right-nav>li>a:hover {
                text-decoration: none;
            }

            .mobile-product img {
                width: 100px;
                margin: 25px;
            }

            .inner {
                padding: 0 50px;
                font-size: 30px;
                text-align: left;
            }

            .kmj-home {}

            .kmj-about {
                padding: 25px;
            }

            .kmj-about p {
                font-size: 14px;
            }

            .container-navbar-footer {
                display: none;
                visibility: hidden;
            }
        }

        @media screen and (max-width: 350px) {
            .navbar-default {
                display: none;
                visibility: hidden;
            }

            .mobile-nav>.full-width-nav img {
                width: 17%;
            }

            .mobile-nav ul.right-nav>li {
                padding-top: 12px;
            }

            .mobile-nav ul.right-nav>li>a {
                font-size: 13px;
                padding: 0 5px;
            }

            .mobile-nav ul.right-nav>li>a:hover {
                text-decoration: none;
            }
        }
    </style>

</body>

</html>
