@extends('layouts.admin')
@section('title', 'complaint')

@section('content')

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-94034622-3');
    </script>

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Laporan Siswa</h1>
            </div>
            <div class="section-body">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body p-0">
                                <div class="table-responsive p-3">
                                    <table class="table table-striped" id="user">
                                        <thead>
                                            <th>ID</th>
                                            <th>Kelas Sekolah</th>
                                            <th>Judul Laporan</th>
                                            <th>Detail Laporan</th>
                                            <th>Waktu Kejadian</th>
                                            <th>Tempat Kejadian</th>
                                            <th>Jenis Bullying</th>
                                            <th>Nama Korban</th>
                                            <th>Kelas</th>
                                            <th>Nama Pelapor</th>
                                            <th>Bukti</th>

                                            <th>responses</th>
                                            <th class="d-print-none">verification</th>
                                            </tr>
                                            @foreach ($complaint as $i => $sis)
                                                <tr>
                                                    <td>{{ $sis->id }}</td>
                                                    <td>{{ $sis->school_class }}</td>
                                                    <td>{{ $sis->report_title }}</td>
                                                    <td>{{ $sis->report_detail }}</td>
                                                    <td>{{ date('d-m-y', strtotime($sis->incident_time)) }}</td>
                                                    <td>{{ $sis->place }}</td>
                                                    <td>{{ $sis->type_of_bullying }}</td>
                                                    <td>{{ $sis->victim_name }}</td>
                                                    <td>{{ $sis->class }}</td>
                                                    <td>{{ $sis->reporter_name }}</td>
                                                    <td>{{ $sis->proof }}</td>

                                                    <td>
                                                        @if ($sis->responses == 'Pending')
                                                            <a href="{{ route('edit-siswa', $sis->id) }}"
                                                                class="btn btn-secondary">{{ $sis->responses }}</a>
                                                        @elseif($sis->responses == 'Accepted')
                                                            <a href="#"
                                                                class="btn btn-success">{{ $sis->responses }}</a>
                                                        @else
                                                            <a href="#"
                                                                class="btn btn-outline-dark">{{ $sis->responses }}</a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($sis->verification == 'Pending')
                                                            <a href="{{ url('', $sis->id) }}"
                                                                class="btn btn-primary">{{ $sis->verification }}</a>
                                                        @elseif($sis->verification == 'Accepted')
                                                            <a href="#"
                                                                class="btn btn-success">{{ $sis->verification }}</a>
                                                        @else
                                                            <a href="#"
                                                                class="btn btn-outline-dark">{{ $sis->verification }}</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <div class="card-tools ml-auto">
                                        <button onclick="window.print()" class="btn btn-default"><i
                                                class="fas fa-print"></i>
                                            Print</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    </div>
    </div>

    @push('addon-script')
        <script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.3/js/dataTables.bootstrap5.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @endpush

@endsection
