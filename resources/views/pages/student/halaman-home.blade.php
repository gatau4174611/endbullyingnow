<!DOCTYPE html>
<html>

<head>
    <title>Halaman Home Pengaduan</title>
    <link rel="stylesheet" href="{{ asset('css/style.css ')}}">
</head>

<body>
    <header>
        <h1>Laporkan Pengaduanmu!</h1>
        <nav>
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">Tentang Ara!</a></li>
                <li><a href="#">Lapor!</a></li>
                <li>
                </li>
                @if (Route::has('login'))
                <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right">
                    @auth
                        <a href="{{ url('/home') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>
                        
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            </ul>
        </nav>
    </header>
    <main>
        <section>
            <h2>Laporan Pengaduan Bully Anda</h2>
            <form action="{{ route('laporan-bully')}}" method="post" enctype="multipart/form-data">
              {{ csrf_field()}}
              <div class="form-group">
                <input type="text" id="school_class" name="school_class" class="form-control" placeholder="school_class"> 
                </div>
              <div class="form-group">
                <input type="text" id="report_title" name="report_title" class="form-control" placeholder="report_title"> 
                </div>
                <div class="form-group">
                  <textarea name="report_detail" id="report_detail"  class="form-control" placeholder="report_detail"></textarea>
                  </div>
                <div class="form-group">
                  <input type="date" id="incident_time" name="incident_time" class="form-control" placeholder="incident_time"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="place" name="place" class="form-control" placeholder="place"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="type_of_bullying" name="type_of_bullying" class="form-control" placeholder="type_of_bullying"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="victim_name" name="victim_name" class="form-control" placeholder="victim_name"> 
                  </div>
                <div class="form-group">
                    <input type="text" id="class" name="class" class="form-control" placeholder="class"> 
                  </div>
                <div class="form-group">
                  <input type="text" id="reporter_name" name="reporter_name" class="form-control" placeholder="reporter_name"> 
                  </div>
                <div class="form-group">
                    <label for="">Proof</label>
                  <input type="file" id="proof" name="proof"> 
                  </div>
                <div class="form-group">
                  <textarea name="photo_desription" id="photo_desription"  class="form-control" placeholder="photo_desription"></textarea>
                  </div>
                
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
              </form>
        </section>
    </main>
    <section class="section-footer">
      <footer>
        <div class="container">
        <div class="row row-sub-footer">
          <div class="col-12 col-md-6 col-lg-3 ps-1">
            <div class="wrapper-col-1 mb-5">
              <h1>Ara Application</h1>
              <p class="mb-4">Lorem ipsum dolor sit amet, <br> consectetur adipiscing elit, sed do <br> elusmod tempor incididunt ut labore<br> et dolore magna aliqua. Ut enim ad</p>
              <div class="wrapper-icon d-flex">
                <a href="#"><img src="/public/image/facebook.png" alt=""></a>
                <a href="#"><img src="/public/image/twitter.png" alt=""></a>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-3 ps-5">
            <div class="wrapper-col-2 mb-5">
              <h1>Useful links</h1>
              <a href="#">Blog</a>
              <a href="#">Hewan</a>
              <a href="#">Galeri</a>
              <a href="#">Testimonial</a>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-3">
            <div class="wrapper-col-3 mb-5">
              <h1>Privacy</h1>
              <a href="#">Karir</a>
              <a href="#">Tentang Kami</a>
              <a href="#">Kontak kami</a>
              <a href="#">Servis</a>
            </div>
          </div>
          <div class="col-12 col-md-6 col-lg-3 mb-5">
            <div class="wrapper-col-4">
              <h1>Contact Info</h1>
              <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="18.829" height="14.834" viewBox="0 0 18.829 14.834">
              <defs>
                <style>
                  .cls-1 {
                    fill: none;
                    stroke: #fff;
                    stroke-linecap: round;
                    stroke-linejoin: round;
                    stroke-width: 2px;
                  }
                </style>
              </defs>
              <g id="mail" transform="translate(-0.607 -3)">
                <path id="Path_25" data-name="Path 25" class="cls-1" d="M3.6,4H16.439a1.609,1.609,0,0,1,1.6,1.6V15.23a1.609,1.609,0,0,1-1.6,1.6H3.6A1.609,1.609,0,0,1,2,15.23V5.6A1.609,1.609,0,0,1,3.6,4Z"/>
                <path id="Path_26" data-name="Path 26" class="cls-1" d="M18.043,6l-8.022,5.615L2,6" transform="translate(0 -0.396)"/>
              </g>
            </svg> tropisianimal@gmail.com </a>
              <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="18.044" height="18.077" viewBox="0 0 18.044 18.077">
                <defs>
                  <style>
                    .cls-1 {
                      fill: none;
                      stroke: #fff;
                      stroke-linecap: round;
                      stroke-linejoin: round;
                      stroke-width: 2px;
                    }
                  </style>
                </defs>
                <path id="phone" class="cls-1" d="M18.155,14.035v2.42A1.613,1.613,0,0,1,16.4,18.068a15.963,15.963,0,0,1-6.961-2.476,15.73,15.73,0,0,1-4.84-4.84A15.963,15.963,0,0,1,2.118,3.758,1.613,1.613,0,0,1,3.724,2h2.42A1.613,1.613,0,0,1,7.757,3.387a10.357,10.357,0,0,0,.565,2.267,1.613,1.613,0,0,1-.363,1.7L6.934,8.381a12.906,12.906,0,0,0,4.84,4.84L12.8,12.2a1.613,1.613,0,0,1,1.7-.363,10.357,10.357,0,0,0,2.267.565,1.613,1.613,0,0,1,1.387,1.637Z" transform="translate(-1.111 -1)"/>
              </svg>
               +62 812 3456 7890</a>
              <a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="15.128" height="18.045" viewBox="0 0 15.128 18.045">
              <defs>
                <style>
                  .cls-1 {
                    fill: none;
                    stroke: #fff;
                    stroke-linecap: round;
                    stroke-linejoin: round;
                    stroke-width: 2px;
                  }
                </style>
              </defs>
              <g id="map-pin" transform="translate(-2)">
                <path id="Path_27" data-name="Path 27" class="cls-1" d="M16.128,7.564c0,5.105-6.564,9.481-6.564,9.481S3,12.669,3,7.564a6.564,6.564,0,1,1,13.128,0Z"/>
                <circle id="Ellipse_17" data-name="Ellipse 17" class="cls-1" cx="2.5" cy="2.5" r="2.5" transform="translate(7 5.046)"/>
              </g>
            </svg>
             Kota Bandung, Jawa Barat</a>
            </div>
          </div>
        </div>
        <div class="wrapper-last-footer">
          <div class="copyright text-white" style="text-align: center;">Copyright @2020 All righs reserved</div>
        </div>
      </div>
      <div class="py-3">
      </div>
      </footer>
    </section>
</body>
</html>
