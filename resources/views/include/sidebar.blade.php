<div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{ route('home')}}">EndBullyingNow</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">EBN</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li><a class="nav-link" href="{{ route('laporan-bully')}}"><span>Daftar Laporan</span></a></li>
            
            <li class="nav-item">
              <a href="{{ route('data-siswa')}}" class="nav-link"><span>Data Siswa</span></a>
            </li>
            <li class="nav-item">
              <a href="{{ route('tanggapan-petugas')}}" class="nav-link"><span>Tanggapan Petugas</span></a>
            </li>
        </aside>
  </div>