  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ url('../assets/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('../assets/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ url('../assets/modules/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="{{ url('../assets/modules/jquery-selectric/selectric.css') }}">
  <link rel="stylesheet" href="{{ url('../assets/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ url('../assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ url('../assets/css/components.css') }}">
