<ul class="navbar-nav navbar-right">
    <li class="dropdown"><a href="#" data-toggle="dropdown"
            class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="{{ asset('../assets/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi</div>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <form action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit btn-danger" style="margin-left: 50px;">Logout</button>
            </form>
        </div>
    </li>
</ul>
