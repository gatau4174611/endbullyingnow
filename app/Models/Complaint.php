<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;

    protected $table = 'complaints';

    protected $primaryKey = 'id';

    protected $fillable = [
        'school_class',
        'report_title',
        'report_detail',
        'incident_time',
        'place',
        'type_of_bullying',
        'victim_name',
        'class',
        'reporter_id',
        'proof',
        'responses',
        'verification',
        'created_at',
        'updated_at',
    ];
}
