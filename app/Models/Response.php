<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    use HasFactory;

    protected $table = 'responses';

    protected $primaryKey = 'id_responses';

    protected $fillable = [
        'complaints_id',
        'responses_id',
        'detail',
        'created_at',
        'updated_at',
    ];
}

