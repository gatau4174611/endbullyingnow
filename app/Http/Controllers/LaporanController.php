<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Complaint;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $complaint = Complaint::all();
        return view('pages.student.laporan-bully', compact('complaint'));
    }

    public function cetaklaporan()
    {
        $cetakcomplaint = Complaint::all();
        return view('pages.student.cetak-laporan');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $type_of_bullying = ['Physical bullying', 'Verbal bullying', 'Social bullying', 'Cyberbullying', 'Sexual bullying', 'Racial bullying', 'Disability bullying', 'Religious bullying', 'Workplace bullying', 'Academic bullying'];
        $class = ['TK', 'SD', 'SMP', 'SMK', 'Unknown'];
        $gender = ['male', 'female'];
        $users = User::all();

        return view('pages.student.create-pengaduan', [
            'type_of_bullying' => $type_of_bullying,
            'class' => $class,
            'gender' => $gender,
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'school_class' => 'required',
            'report_title' => 'required',
            'detail' => 'required',
            'incident_time' => 'required',
            'place' => 'required',
            'type_of_bullying' => 'required',
            'victim_name' => 'required',
            'reporter_id' => 'required',
            'proof' => 'required|file',

        ]);

        $validatedData['report_detail'] = $validatedData['detail'];
        $store = Complaint::create($validatedData);

        if ($store) :
            alert()->success('Berhasil', 'Laporan Berhasil Dikirim');
        else :
            alert()->error('Terjadi Kesalahan', 'Laporan Gagal Dikirim');
        endif;

        return redirect()->route('laporan-bully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Complaint::findOrFail($id);
        $data->delete();
        return back()->with('info', 'Data berhasil dihapus');
    }
}
