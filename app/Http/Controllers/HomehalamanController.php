<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $complaint = Complaint::all();
        return view('pages.student.laporan-bully', compact('complaint'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.student.halaman-home');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        Complaint::create([
            'school_class' => $request->school_class,
            'report_title' => $request->report_title,
            'report_detail' => $request->report_detail,
            'incident_time' => $request->incident_time,
            'place' => $request->place,
            'type_of_bullying'=> $request->type_of_bullying,
            'victim_name'=> $request->victim_name,
            'class'=> $request->class,
            'reporter_name'=> $request->reporter_name,
            'proof'=> $request->proof,
            'photo_desription'=> $request->photo_desription,
        ]);
  
        return redirect('halaman-home')->with('toast_success', 'Kamu berhasil Melapor');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Complaint::findOrFail($id);
        $data->delete();
        return back()->with('info', 'Data berhasil dihapus');
    }
}
