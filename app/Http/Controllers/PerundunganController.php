<?php

namespace App\Http\Controllers;

use App\Models\Complaint;
use Illuminate\Http\Request;

class PerundunganController extends Controller
{
    public function update(Request $request, $id)
{
    $perundungan = Complaint::findOrFail($id);
    if($request->input('verification')){
        $perundungan->verification = $request->input('verification');
    }
    if($request->input('responses')){
        $perundungan->responses = $request->input('responses');
    }
    
    $perundungan->save();
    return redirect()->route('laporan-bully')->with('success', 'Perundungan updated successfully');

}

}
