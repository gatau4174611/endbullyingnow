<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Complaint;
use App\Models\User;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Hash;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $complaint = Complaint::orderBy('id', 'asc')->get();

        return view('pages.admin.complaint.index', ['complaint' => $complaint]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $type_of_bullying = ['Physical bullying', 'Verbal bullying', 'Social bullying', 'Cyberbullying', 'Sexual bullying', 'Racial bullying', 'Disability bullying', 'Religious bullying', 'Workplace bullying', 'Academic bullying'];
        $class = ['TK', 'SD', 'SMP', 'SMK', 'Unknown'];
        $gender = ['male', 'female'];
        $users = User::all();

        return view('pages.admin.complaint.create', [
            'type_of_bullying' => $type_of_bullying,
            'class' => $class,
            'gender' => $gender,
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'school_class' => 'required',
            'report_title' => 'required',
            'detail' => 'required',
            'incident_time' => 'required',
            'place' => 'required',
            'type_of_bullying' => 'required',
            'victim_name' => 'required',
            'reporter_id' => 'required',
            'proof' => 'required|file',

        ]);

        $validatedData['report_detail'] = $validatedData['detail'];
        $store = Complaint::create($validatedData);

        if ($store) :
            alert()->success('Berhasil', 'Laporan Berhasil Dikirim');
        else :
            alert()->error('Terjadi Kesalahan', 'Laporan Gagal Dikirim');
        endif;

        return redirect()->route('laporan-bully');
    }



    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($status)
    {
        $complaint = Complaint::find($status);
        $complaint = Complaint::all();
        $users = User::all();

        return view('pages.admin.complaint.edit', [
            'complaint' => $complaint,
            'type_of_bullying' => $complaint,
            'status' => $complaint,
            'gender' => $users,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $title)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($email)
    {
        if (Complaint::find($email)->delete()) :
        // Alert::success('Berhasil', 'Data Berhasil di Hapus');
        else :
        //Alert::error('Terjadi Kesalahan', 'Data Gagal di Hapus');
        endif;

        return back();
    }
}
