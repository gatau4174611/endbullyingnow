<?php

use App\Http\Controllers\Admin\ComplaintController;
use App\Http\Controllers\LogoutController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('/data-complaint', ComplaintController::class);

// Route::resource('/data-responses.index', ComplaintController::class);



Route::post('/logout', [LogoutController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');

Route::get('/Student-login', [App\Http\Controllers\Auth\SiswaLoginController::class, 'showLoginForm'])->name('Student-login');
Route::get('/Student-login', [App\Http\Controllers\Auth\SiswaLoginController::class, 'login'])->name('Student-login');
Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');



Route::get('/create-siswa', [App\Http\Controllers\DatasiswaController::class, 'create'])->name('create-siswa');
Route::post('/simpan-siswa', [App\Http\Controllers\DatasiswaController::class, 'store'])->name('simpan-siswa');
Route::get('/edit-siswa/{id}', [App\Http\Controllers\DatasiswaController::class, 'edit'])->name('edit-siswa');
Route::post('/update-siswa/{id}', [App\Http\Controllers\DatasiswaController::class, 'update'])->name('update-siswa');
Route::delete('delete-siswa/{id}', [App\Http\Controllers\DatasiswaController::class, 'destroy'])->name('delete-siswa');


Route::delete('/delete-laporan/{id}', [App\Http\Controllers\LaporanController::class, 'destroy'])->name('delete-laporan');
Route::post('/laporan-bully', [App\Http\Controllers\LaporanController::class, 'index'])->name('laporan-bully');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/data-siswa', [App\Http\Controllers\DatasiswaController::class, 'index'])->name('data-siswa');
Route::get('/laporan-bully', [App\Http\Controllers\HomehalamanController::class, 'index'])->name('halaman-home');
Route::get('/simpan-laporan', [App\Http\Controllers\HomehalamanController::class, 'store'])->name('halaman-home');
Route::get('/halaman-home', [App\Http\Controllers\HomehalamanController::class, 'create'])->name('halaman-home');
Route::get('/logout', [App\Http\Controllers\LoginController::class, 'logout']);

Route::get('/create-pengaduan', [App\Http\Controllers\LaporanController::class, 'create'])->name('create-pengaduan');
Route::get('/laporan-bully', [App\Http\Controllers\LaporanController::class, 'index'])->name('laporan-bully');
Route::post('/simpan-laporan', [App\Http\Controllers\LaporanController::class, 'store'])->name('simpan-laporan');

Route::get('/tanggapan-petugas', [App\Http\Controllers\ResponsController::class, 'index'])->name('tanggapan-petugas');
Route::get('/cetak-laporan', [App\Http\Controllers\LaporanController::class, 'cetaklaporan'])->name('cetak-laporan');

// controller buat edit//
Route::put('/perundungan/{id}/update', [App\Http\Controllers\PerundunganController::class, 'update'])->name('perundungan.update');

Route::get('/about', function () {
    return view('about');
});
